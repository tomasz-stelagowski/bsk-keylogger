#pragma once

#ifdef KEYLOGLIBRARY_EXPORTS
#define KEYLOGLIBRARY_API __declspec(dllexport)
#else
#define KEYLOGLIBRARY_API __declspec(dllimport)
#endif

void WINAPI ServiceMain(void);